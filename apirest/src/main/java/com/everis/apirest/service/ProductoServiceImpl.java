package com.everis.apirest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.apirest.model.entity.Producto;
import com.everis.apirest.model.repository.ProductoRepository;

@Service
public class ProductoServiceImpl implements ProductoService{

	
	@Autowired
	private ProductoRepository productoRepository;
	
	@Override
	public Iterable<Producto> obtenerProductos() {
		
		return productoRepository.findAll();
	}

	@Override
	public Producto insertar(Producto producto) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
