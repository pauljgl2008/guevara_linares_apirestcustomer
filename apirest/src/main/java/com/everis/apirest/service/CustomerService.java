package com.everis.apirest.service;

import com.everis.apirest.controller.resource.CustomerReducidoResource;
import com.everis.apirest.model.entity.Customer;

public interface CustomerService {
		
	public Iterable<Customer> obtenerCustomers();
	
	public Customer guardar(CustomerReducidoResource customer);
}
