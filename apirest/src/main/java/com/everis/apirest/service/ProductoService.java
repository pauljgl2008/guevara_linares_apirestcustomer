package com.everis.apirest.service;

import com.everis.apirest.model.entity.Producto;

public interface  ProductoService {
		
	public Iterable<Producto> obtenerProductos();
	
	public Producto insertar(Producto producto);
}
