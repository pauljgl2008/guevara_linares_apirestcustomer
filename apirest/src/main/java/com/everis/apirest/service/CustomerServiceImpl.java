package com.everis.apirest.service;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.apirest.controller.resource.CustomerReducidoResource;
import com.everis.apirest.model.entity.Customer;
import com.everis.apirest.model.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService{

	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Override
	public Iterable<Customer> obtenerCustomers() {
		
		return customerRepository.findAll();
	}

	@Override
	public Customer guardar(CustomerReducidoResource customer) {
		Customer a = new Customer();
		a.setName(customer.getNombre());
		
		String apellidos = customer.getApellidoCompleto();
		apellidos = apellidos.trim();
		String[] arr = apellidos.split("\\s++");
	    if(arr.length>1) {
	    	a.setLastName(arr[0]);
			a.setLastName2(arr[1]);
		}
		else{
			a.setLastName(apellidos);
		}
		
		return customerRepository.save(a);
	}

	
}
