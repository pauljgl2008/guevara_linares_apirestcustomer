package com.everis.apirest.controller.resource;

import lombok.Data;


@Data
//@Getter
//@Setter
//@AllArgsConstructor
//@NoArgsConstructor

public class ProductoResource {
	private Long id;
	private String nombre;
	private String descripcion;
}
