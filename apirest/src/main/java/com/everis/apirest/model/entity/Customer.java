package com.everis.apirest.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name="customer")

public class Customer {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	 /*@Id
	    @SequenceGenerator(name="customer_idcustomer_seq",
	                       sequenceName="customer_idcustomer_seq",
	                       allocationSize=1)
	    @GeneratedValue(strategy = GenerationType.SEQUENCE,
	                    generator="customer_idcustomer_seq")
	    @Column(name = "id", updatable=false)*/
	private Long id;
	private String name;
	private String lastName;
	private String lastName2;
}
